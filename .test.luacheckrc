codes = true
read_globals = {
    -- Test.More
    'plan',
    'done_testing',
    'skip_all',
    'BAIL_OUT',
    'subtest',
    'diag',
    'note',
    'skip',
    'todo_skip',
    'skip_rest',
    'todo',
    -- Test.Assertion
    'equals',
    'array_equals',
    'is_string',
    'is_table',
    'truthy',
    'matches',
    'error_matches',
    'require_ok',
    -- testsuite
    'Person',
    'Friend',
    'Avatar',
    'Car',
}
globals = {
    -- Coat
    'argerror',
    'checktype',
    'can',
    'isa',
    'does',
    'dump',
    'new',
    'instance',
    '_INIT',
    'has',
    'method',
    'overload',
    'override',
    'mock',
    'unmock',
    'before',
    'around',
    'after',
    'memoize',
    'bind',
    'extends',
    'with',
    'module',
    'class',
    'singleton',
    'abstract',
    'augment',
    -- Coat.Role
    'requires',
    'excludes',
    'role',
    -- Coat.Types
    'find_type_constraint',
    'coercion_map',
    'subtype',
    'enum',
    'coerce',
    -- Coat.Persistent
    'establish_connection',
    'connection',
    'save',
    'delete',
    'create',
    'find_by_sql',
    'find',
    'has_p',
    'has_one',
    'has_many',
    'persistent',
}

ignore = { '111/sql_create' }
files['test/002-subobjects.t'].ignore = { '122/Person', '631' }
files['test/016-options.t'].ignore = { '122/Person' }

