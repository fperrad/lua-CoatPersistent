
lua-CoatPersistent : an ORM for lua-Coat
========================================

Introduction
------------

lua-CoatPersistent is a Lua 5.1 port of [Coat::Persistent](https://metacpan.org/pod/Coat::Persistent),
a Perl module, which is inspired by Ruby on Rails Active Record.

lua-CoatPersistent is an Object-Relational Mapping for [lua-Coat](https://fperrad.frama.io/lua-Coat).
It is built over the modules [LuaSQL](https://keplerproject.github.io/luasql/)
and [Dado](http://www.ccpa.puc-rio.br/software/dado).
It could support all database engine which has a driver in LuaSQL.

Another variant is built over the module [lsqlite3](http://lua.sqlite.org/)
instead of LuaSQL.

Links
-----

The homepage is at <https://fperrad.frama.io/lua-CoatPersistent>,
and the sources are hosted at <https://framagit.org/fperrad/lua-CoatPersistent>.

Copyright and License
---------------------

Copyright (c) 2010-2018 Francois Perrad

This library is licensed under the terms of the MIT/X11 license, like Lua itself.

