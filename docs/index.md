
# lua-CoatPersistent

---

## Overview

lua-CoatPersistent is an Object-Relational Mapping
for [lua-Coat](https://fperrad.frama.io/lua-Coat).
It is built over the modules
[LuaSQL](https://keplerproject.github.io/luasql/) and
[Dado](http://www.ccpa.puc-rio.br/software/dado).
It could support all database engine which has a driver in LuaSQL.

Another variant is built over the module
[lsqlite3](http://lua.sqlite.org/) instead of LuaSQL.

## Lineage

lua-CoatPersistent is a Lua port of
[Coat::Persistent](https://metacpan.org/pod/Coat::Persistent)
, a Perl module, which is inspired by Ruby on Rails
[Active Record](https://rubyonrails.org/).

## Status

lua-CoatPersistent is in early stage.

It's developed for Lua 5.1, 5.2 & 5.3.

## Download

lua-CoatPersistent source can be downloaded from
[Framagit](https://framagit.org/fperrad/lua-CoatPersistent).

## Installation

Two variants are available, LuaSQL or lsqlite3 based

lua-CoatPersistent is available via LuaRocks:

```sh
luarocks install lua-coatpersistent-luasql
# luarocks install lua-coatpersistent-lsqlite3
```

or manually, with:

```sh
make install.luasql
# make install.lsqlite3
```

## Test

The test suite requires the module
[lua-TestAssertion](https://fperrad.frama.io/lua-TestAssertion/)
and LuaSQL-SQLite3.

```sh
make test.luasql
# make test.lsqlite3
```

## Copyright and License

Copyright &copy; 2010-2018 Fran&ccedil;ois Perrad

This library is licensed under the terms of the MIT/X11 license,
like Lua itself.
