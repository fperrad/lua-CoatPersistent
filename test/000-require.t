#!/usr/bin/env lua

require 'Test.Assertion'

plan(8)

if not require_ok 'Coat.Persistent' then
    BAIL_OUT "no lib"
end

diag(require'dado'._VERSION)

local m = require 'Coat.Persistent'
is_table( m )
equals( m, package.loaded['Coat.Persistent'] )
matches( m._COPYRIGHT, 'Perrad', "_COPYRIGHT" )
matches( m._DESCRIPTION, 'ORM', "_DESCRIPTION" )
is_string( m._VERSION, "_VERSION" )
matches( m._VERSION, '^%d%.%d%.%d$' )

equals( m.math, nil, "check ns pollution" )

