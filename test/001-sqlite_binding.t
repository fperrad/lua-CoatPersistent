#!/usr/bin/env lua

require 'Coat.Persistent'

persistent 'Person'

has_p.name = { is = 'rw', isa = 'string' }
has_p.age = { is = 'rw', isa = 'number' }

sql_create = [[
    CREATE TABLE person (
        id INTEGER,
        name CHAR(64),
        age INTEGER
    )
]]

require 'Test.Assertion'

plan(19)
require 'Coat.Persistent'.trace = print

if os.getenv "GEN_PNG" and os.execute "dot -V" == 0 then
    local f = io.popen("dot -T png -o 001.png", 'w')
    f:write(require 'Coat.UML'.to_dot())
    f:close()
end

local mc = require 'Coat.Meta.Class'

os.remove 'test.db'
local conn = Person.establish_connection('sqlite3', 'test.db')
conn:execute(Person.sql_create)

truthy( mc.has( Person, 'id' ), "field id" )
truthy( mc.has( Person, 'name' ), "field name" )
truthy( mc.has( Person, 'age' ), "field age" )

local john = Person { name = 'John', age = 23 }
equals( john:type(), 'Person', "Person" )
truthy( john:isa 'Person' )

equals( john.id, nil, "john.id is nil" )
truthy( john:save(), "john:save() --> insert" )
equals( john.id, 1, "john.id is 1" )

local brenda = Person { name = 'Brenda', age = 22 }
equals( brenda.id, nil, "brenda.id is nil" )
truthy( brenda:save(), "brenda:save()" )
equals( brenda.id, 2, "brenda.id is 2" )

local p = Person.find(1)()
truthy( p, "Person.find(1) returns something" )
truthy( p:isa 'Person', "it is a Person" )
equals( p.name, 'John', "it is John" )
equals( p.age, 23 )

p = Person.find_by_name('Brenda')()
truthy( p, "Person.find_by_name returns something" )
truthy( p:isa 'Person', "it is a Person" )
equals( p.name, 'Brenda', "it is Brenda" )

truthy( brenda:delete(), "brenda:delete()" )

